# Install Genie v3.20 on BeoShock

This sample project shows how to install Genie v3.20 on BeoShock to your home directory. The source files for dependecies are included in this repository. Before installation, you need to load several modules. If you want to use other version of `GCC`, all modules need to change the version number corresponding to the GCC version.

    module load GCC/9.3.0
    module load CMake/3.16.4-GCCcore-9.3.0
    module load libxml2/2.9.10-GCCcore-9.3.0
    module load X11/20200222-GCCcore-9.3.0
    module load GSL/2.6-GCC-9.3.0
    module load OpenSSL/1.1
    module load Autoconf/2.69-GCCcore-9.3.0

There are also ROOT and Genie setup scripts can be used for installation.

## Install LHAPDF6
The source files can be downloaded from [here](https://lhapdf.hepforge.org/downloads/) or you can use the LHAPDF-6.5.1.tar.gz
    
    mkdir lhapdf_install
    wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.X.Y.tar.gz -O LHAPDF-6.X.Y.tar.gz
    tar xf LHAPDF-6.X.Y.tar.gz
    cd LHAPDF-6.X.Y
    ./configure --prefix=/path/for/lhapdf_install
    make
    make install

## Install log4cpp
Extract the log4cpp-1.1.3.tar.gz and install

    mkdir log4cpp_install
    tar xf log4cpp-1.1.3.tar.gz
    cd log4cpp
    ./configure --prefix=/path/for/log4cpp_install
    make 
    make install

## Install Pythia6
`build_pythia6.sh` can be used to build Pythia6, just copy `build_pythia6.sh` to your Pythia6 installation directory and run the script.

## Install ROOT 
Here I only enable the Pythia6 and the required options for ROOT. All build options can be found [here](https://root.cern/install/build_from_source/#all-build-options)
    
    tar xf root_v6.26.02.source.tar.gz
    mkdir root_install root_build
    cd root_build
    cmake -DCMAKE_INSTALL_PREFIX=/path/for/root_install /path/for/root-6.26.02 -DPYTHIA6_LIBRARY=/path/for/pythia6/lib/libPythia6.so -Dxrootd=Off -Dvdt=Off -Dtmva-cpu=Off -Dtmva=off -Dbuiltin_tbb=Off -Dminuit2=Off -Dbuiltin_openui5=Off -Druntime_cxxmodules=Off -Dgdml=Off -Dimt=Off -Droofit=Off -Dasimage=Off -Dwebgui=Off -Dhttp=Off -Dtmva-pymva=Off -Dpyroot=Off -Ddataframe=Off

Then build the ROOT. If you check out multi core oN BeoShock, you can speed up the build with `-jN`, where N is the number of aviable cores.
    
    cmake --build . --target install -jN

To configure and build ROOT, you can also copy the script `root_setup.sh` and `root_build.sh` to the `root_build` directory and run:
    
    cd root_build
    ./root_setup.sh
    ./root_build.sh

## Install Genie
Clone the Genie source code from git and change the v3.20:
    
    git clone https://github.com/GENIE-MC/Generator.git
    cd Generator
    git tag -l
    git checkout -b R-3_02_00-br R-3_02_00

Before the installation, you need to setup the library and binary path. You need to use the full path to link the library for lhapdf, ROOT, libxml2, log4cpp, and Pythia6. 

    export GENIE=$HOME/Generator
    export ROOTSYS=/path/to/root_install
    export LHAPATH=/path/to/lhapdf_install/share/LHAPDF
    export PATH=$PATH:\
    $ROOTSYS/bin:\
    $GENIE/bin

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:\
    /path/to/log4cpp_install/lib:\
    /opt/software/software/libxml2/2.9.10-GCCcore-9.3.0/lib:\
    /path/to/lhapdf_install/lib:\
    /path/to/pythia6/lib:\
    $ROOTSYS/lib:\
    $GENIE/lib

Then configure and build the Genie. Again, please change the path corresponding to your installation directory. 

    ./configure --prefix=$HOME/Genie_install --disable-profiler --disable-validation-tools --disable-cernlib --disable-lhapdf5 --enable-lhapdf6 --enable-flux-drivers --enable-geom-drivers --disable-doxygen --enable-test --enable-mueloss --enable-dylibversion --enable-t2k --enable-fnal --enable-atmo --enable-nucleon-decay --disable-masterclass --disable-debug --with-optimiz-level=O2 --with-pythia6-lib=$HOME/Genie-dependencies/v6_428/lib --with-lhapdf6-inc=$HOME/Genie-dependencies/lhapdf_install/include --with-lhapdf6-lib=$HOME/Genie-dependencies/lhapdf_install/lib --with-libxml2-inc=/opt/software/software/libxml2/2.9.10-GCCcore-9.3.0/include --with-libxml2-lib=/opt/software/software/libxml2/2.9.10-GCCcore-9.3.0/lib --with-log4cpp-inc=$HOME/Genie-dependencies/log4cpp_install/include --with-log4cpp-lib=$HOME/Genie-dependencies/log4cpp_install/lib
    gmake -jN

You can use `genie_setup.sh`. Replace the path for log4cpp, lhapdf, pythia6, root and genie source directory. Then 

    source genie_setup.sh
    cd $GENIE
    ./configure --prefix=$HOME/Genie_install --disable-profiler --disable-validation-tools --disable-cernlib --disable-lhapdf5 --enable-lhapdf6 --enable-flux-drivers --enable-geom-drivers --disable-doxygen --enable-test --enable-mueloss --enable-dylibversion --enable-t2k --enable-fnal --enable-atmo --enable-nucleon-decay --disable-masterclass --disable-debug --with-optimiz-level=O2 --with-pythia6-lib=$HOME/Genie-dependencies/v6_428/lib --with-lhapdf6-inc=$HOME/Genie-dependencies/lhapdf_install/include --with-lhapdf6-lib=$HOME/Genie-dependencies/lhapdf_install/lib --with-libxml2-inc=/opt/software/software/libxml2/2.9.10-GCCcore-9.3.0/include --with-libxml2-lib=/opt/software/software/libxml2/2.9.10-GCCcore-9.3.0/lib --with-log4cpp-inc=$HOME/Genie-dependencies/log4cpp_install/include --with-log4cpp-lib=$HOME/Genie-dependencies/log4cpp_install/lib
    gmake -jN

## Test after installations
Extract the pre-computed cross-section files `genie_xsec-3.02.00-noarch-G1802a00000-k250-e1000.tar.bz2`,

    tar xf genie_xsec-3.02.00-noarch-G1802a00000-k250-e1000.tar.bz2

Generate a 1k event sample of nu_mu+O^16 interactions between 0 and 10 GeV using a simple analytical (x*exp(-x)) numu flux description. You may need to load the modules and export path if you open an new terminal.
    
    source genie_setup.sh
    gevgen -n 1000 -p 14 -t 1000080160 -e 0,10 -f 'x*exp(-x)' --run 1000 --seed 1721827 --cross-sections /path/for/genie_xsec/v3_02_00/NULL/G1802a00000-k250-e1000/data/gxspl-FNALsmall.xml --tune G18_02a_00_000

Print-out the first 50 events from the GHEP:

    gevdump -n 50 -f gntp.0.ghep.root

Generate 1k events: 

    gevgen_hadron -n 1000 -p 211 -t 1000080160 -k 0.2 --seed 65431

    
 

















