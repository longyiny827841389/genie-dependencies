#!/bin/bash

module purge
module load GCC/9.3.0
module load CMake/3.16.4-GCCcore-9.3.0
module load libxml2/2.9.10-GCCcore-9.3.0
module load X11/20200222-GCCcore-9.3.0
module load GSL/2.6-GCC-9.3.0
module load OpenSSL/1.1
module load Autoconf/2.69-GCCcore-9.3.0

cmake -DCMAKE_INSTALL_PREFIX=$HOME/Genie-dependencies/root_install $HOME/Genie-dependencies/root-6.26.02 -DPYTHIA6_LIBRARY=$HOME/Genie-dependencies/v6_428/lib/libPythia6.so -Dxrootd=Off -Dvdt=Off -Dtmva-cpu=Off -Dtmva=off -Dbuiltin_tbb=Off -Dminuit2=Off -Dbuiltin_openui5=Off -Druntime_cxxmodules=Off -Dgdml=Off -Dimt=Off -Droofit=Off -Dasimage=Off -Dwebgui=Off -Dhttp=Off -Dtmva-pymva=Off -Dpyroot=Off -Ddataframe=Off

