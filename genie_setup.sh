#!/bin/bash
module purge
module load GCC/9.3.0
module load CMake/3.16.4-GCCcore-9.3.0
module load libxml2/2.9.10-GCCcore-9.3.0
module load X11/20200222-GCCcore-9.3.0
module load GSL/2.6-GCC-9.3.0
module load OpenSSL/1.1
module load Autoconf/2.69-GCCcore-9.3.0

export GENIE=$HOME/Generator
export ROOTSYS=$HOME/Genie-dependencies/root_install_3
export LHAPATH=$HOME/Genie-dependencies/lhapdf_install/share/LHAPDF
export PATH=$PATH:\
$ROOTSYS/bin:\
$GENIE/bin

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:\
$HOME/Genie-dependencies/log4cpp_install/lib:\
/opt/software/software/libxml2/2.9.10-GCCcore-9.3.0/lib:\
$HOME/Genie-dependencies/lhapdf_install/lib:\
$HOME/Genie-dependencies/v6_428/lib:\
$ROOTSYS/lib:\
$GENIE/lib


