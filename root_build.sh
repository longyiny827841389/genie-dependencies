#!/bin/bash

module purge
module load GCC/9.3.0
module load CMake/3.16.4-GCCcore-9.3.0
module load libxml2/2.9.10-GCCcore-9.3.0
module load X11/20200222-GCCcore-9.3.0
module load GSL/2.6-GCC-9.3.0
module load OpenSSL/1.1
module load Autoconf/2.69-GCCcore-9.3.0

cmake --build . --target install -j10


